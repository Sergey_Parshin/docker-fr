# docker-fr



## Подготовка проекта

1. Создать папку проекта ``docker-fr`` и перейти в нее
```
mkdir docker-fr
cd docker-fr
```
2. Клонировать текущий репозиторий в эту папку
```
git clone git@gitlab.com:Sergey_Parshin/docker-fr.git .
```
3. Создать папку ``front`` и перейти в нее
```
mkdir front
cd front
```
4. Клонировать репозиторий модератора в папку ``front``
```
git clone git@gitlab.com:Sergey_Parshin/moderator2.git .
```

## Конфигурация проекта

Конфигурация находится в файле ``docker-compose.yml``

![Конфигурация](env-view.png)

## Запуск проекта
1. в папке ``docker-fr`` выполнить

```
docker-compose up --build -d
```

Будет собран образ приложения и запущен сервер на стандартном 80 порту

## Остановка приложения
1. в папке ``docker-fr`` выполнить

```
docker-compose down
```
