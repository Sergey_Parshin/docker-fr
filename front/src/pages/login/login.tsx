import { SyntheticEvent, useEffect, useState } from "react";
import { useNavigate } from "react-router-dom";
import { useCheckAuthMutation, useLogInMutation } from "../../features";
import { ApiServices } from "../../services/api-services";
import { ApiInfo, Button, Field, Panel } from "../../ui";
import s from "./login.module.css";

export function Login() {
  let navigate = useNavigate();
  const [fetchAuth, response] = useCheckAuthMutation();
  const { isSuccess, isLoading, isError, error } = response;

  useEffect(() => {
    fetchAuth({})
  }, []);

  useEffect(() => {
    if (isSuccess) navigate("/");
  }, [isSuccess])

  // useEffect(() => {
  //   getAuthData()
  //   if (isSuccess) navigate("/");
  //   currentUrl.toString().slice(0,5)
  //   setCurrentUrl(currentUrl)
  // }, [isSuccess])

  return (
    <div />
  );
}
