import { useState } from "react";
import { useThreadsQuery, useTopicsQuery, mergeQueries } from "../../features";
import { Button, PageTitle, Panel, ApiInfo, NoData, Topic } from "../../ui";
import s from "./trending.module.css";

const referenceIdMap = {
  subject: process.env.REACT_APP_SUBJECTS_ID,
  event: process.env.REACT_APP_EVENTS_ID,
};

export const Trending = () => {
  type SateType = "subject" | "event";
  const [type, setType] = useState<SateType>("subject");
  const threadsQuery = useThreadsQuery("");
  const threadId = threadsQuery.data?.[0].id;
  const topicsQuery = useTopicsQuery(
    {
      thread_id: threadId,
      reference_id: referenceIdMap[type],
    },
    { skip: !threadId }
  );

  const { isSuccess, isError, isFetching, error } = mergeQueries(
    threadsQuery,
    topicsQuery
  );
  const hasItems = topicsQuery.data?.items?.length > 0;

  return (
    <>
      <PageTitle>Быстрорастущие темы</PageTitle>
      <Panel className={s.filter} padding>
        <Button active={type === "subject"} onClick={() => setType("subject")}>
          Субъект
        </Button>
        <Button active={type === "event"} onClick={() => setType("event")}>
          Событие
        </Button>
        <Button disabled>Федеральная повестка</Button>
      </Panel>
      <ApiInfo isLoading={isFetching} isError={isError} error={error} />
      {isSuccess && !hasItems && <NoData />}
      {isSuccess && hasItems && (
        <div className={s.list}>
          {topicsQuery.data.items.map((item: any, index: number) => (
            <Topic key={item.id} number={index + 1} data={item} />
          ))}
        </div>
      )}
    </>
  );
};
