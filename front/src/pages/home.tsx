import { useEffect } from "react";
import { useNavigate } from "react-router-dom";

export function Home() {
  let navigate = useNavigate();
  useEffect(() => {
    navigate("/statistic");
  }, []);
  return null;
}
