import clsx from "clsx";
import {
  useStatsQuery,
  useRiskListQuery,
  useSmiTopQuery,
  useSocTopQuery,
  useSmiGraphQuery,
  useSocGraphQuery,
  useUnatedGraphQuery,
  useIndicatorsTopQuery,
  useGetAllPostsQuery,
  useNewsbreakListQuery
} from "../../features";
import { Panel, StatsPanel, Table } from "../../ui";
import { Title } from "../../ui/panel/title";
import { Tooltip, Bar, XAxis, YAxis, Legend } from "recharts";
import { BarChart } from "../../ui";

import s from "./statistic.module.css";
import {Scorecard} from "../../ui/scorecard";
import {TopSources} from "../../ui/top-sources";
import {ApiServices} from "../../services/api-services";
import {useEffect, useState} from "react";

type DataItem = {
  id: number;
  title: number;
  items: any[];
};

const getMonitoringCount = (data: DataItem[], id: string) => {
  const arr = data?.find((item: any) => item.id === +id);
  return arr?.items.length;
};

export function Statistic() {
  const [riskCount, setRiskCount] = useState<number>(0)
  const [materialCount, setMaterialCount] = useState<number>(0)
  const result = useStatsQuery("");
  const subjectsCount = getMonitoringCount(
    result.data,
    process.env.REACT_APP_SUBJECTS_ID as string
  );
  const eventsCount = getMonitoringCount(
    result.data,
    process.env.REACT_APP_EVENTS_ID as string
  );

  const queryData = useRiskListQuery({ start: 0, limit: 0 });
  const allPosts = useGetAllPostsQuery({})
  const infoCount = useNewsbreakListQuery({ start: 0, limit: 0 }).data?.items.length

  useEffect(() => {
    if (queryData?.data?.items) {
      getRisksData()
    }
  }, [queryData])

  useEffect(() => {
    if (allPosts?.data) {
      getMaterialsData()
    }
  }, [allPosts])

  const getRisksData = async () => {
    // let tempRiskCount: number = 0
    // for (const riskItem of queryData.data.items) {
    //   const riskResponse = await ApiServices.getRiskItem({ id: riskItem.id })
    //   if (riskResponse?.data?.status === 'Выполняется') {
    //     tempRiskCount++
    //   }
    // }
    setRiskCount(queryData.data.count)
    // setRiskCount(tempRiskCount)
  }

  const getMaterialsData = async () => {
    const materialsList = []
    for (const item of allPosts.data) {
      const itemResponse = await ApiServices.getThreadItem({ thread_id: +item.id })
      if (itemResponse.data && itemResponse.data.thread_id !== 0) {
        materialsList.push({
          ...item,
          details: itemResponse.data
        })
      }
    }
    setMaterialCount(materialsList.length)
  }

  return (
    <>
      <div className={s.statsGrid}>
        <div className={s.stats}>
          <StatsPanel
            className={s.stat}
            title="Всего субъектов мониторинга"
            count={subjectsCount}
          />
          <StatsPanel
            className={s.stat}
            title="Всего событий мониторинга"
            count={eventsCount}
          />
          <StatsPanel
            className={s.stat}
            title="Всего готовых материалов"
            count={materialCount}
          />
          <StatsPanel
            className={s.stat}
            title="Всего рисков"
            count={riskCount}
          />
          <StatsPanel
            className={s.stat}
            title="Количество инфоповодов в&nbsp;системе"
            count={infoCount || '-'}
          />
        </div>
      </div>

      <Scorecard id={+process.env.REACT_APP_STATS_THREAD_ID!} />
      <TopSources id={+process.env.REACT_APP_STATS_THREAD_ID!} sourceType={'smi'} />
      <TopSources id={+process.env.REACT_APP_STATS_THREAD_ID!} sourceType={'social'} />
    </>
  );
}
