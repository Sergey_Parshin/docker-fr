export const referencesMap = {
  subject: process.env.REACT_APP_SUBJECTS_ID,
  event: process.env.REACT_APP_EVENTS_ID,
};
