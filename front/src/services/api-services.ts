import axios from "axios";

type ApiResponseType = {
  data: any,
  error: any
}

export class ApiServices {
  static async getMaterialItem (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('thread/getthreadextended', params)
  }
  static async getThread (): Promise<ApiResponseType> {
    return await this.baseApiRequest('threads/get', {})
  }
  static async getWeekTrust (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('content/getweektrust', params)
  }
  static async getPostCount (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('content/getpostcount', params)
  }
  static async getRiskList (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('inpd/getRiskList', params)
  }
  static async getRiskItem (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('inpd/getRisk', params)
  }
  static async sendEvent (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('users/messageCreationRequest', params)
  }
  static async getInpdItem (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('inpd/getInpd', params)
  }
  static async getTopicRating (): Promise<ApiResponseType> {
    return await this.baseApiRequest('inpd/getTopicRating', {})
  }
  static async getMainTopics (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('stats/getMainTopics', params)
  }
  static async getTopicRatingShort (): Promise<ApiResponseType> {
    return await this.baseApiRequest('inpd/getTopicRatingShortRange', {})
  }
  static async getTopic (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('content/getTopic', params)
  }
  static async getSourceTopic (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('stats/getReferenceSourceTopics', params)
  }
  static async getThreadItem (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('thread/getthreadextended', params)
  }
  static async getThreadStats (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('stats/getThreadStats', params)
  }
  static async getOwnersTopByPostCount (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('stats/getOwnersTopByPostCount', params)
  }
  static async getAllMembers (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('thread/allmembers', params)
  }
  static async getCurrentSmi (): Promise<ApiResponseType> {
    return await this.baseApiRequest('content/currentsmi', {})
  }
  static async getRefSources (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('inpd/getRefSources', params)
  }
  static async addRefStopPost (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('users/addrefstoppost', params)
  }
  static async removeRefStopPost (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('users/removerefstoppost', params)
  }
  static async setPostTrust (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('content/setposttrust', params)
  }
  static async sendChangeTonalRequest (params: any): Promise<ApiResponseType> {
    return await this.baseApiRequest('users/messageTrustRequest', params)
  }
  static async getAuthCheck (): Promise<ApiResponseType> {
    return await this.baseApiRequest('authorization/login_get', {})
  }

  static async logout (): Promise<ApiResponseType> {
    return await this.baseApiRequest('authorization/logout', {})
  }

  static async baseApiRequest (url: string, params: any): Promise<{ data: any, error: any}> {
    let result = null
    let error = null

    try {
      result = await axios.post(process.env.REACT_APP_API_URL + url, params,{ withCredentials: true })
    } catch (err) {
      error = err
    }

    return {
      data: result?.data,
      error,
    }
  }

}
