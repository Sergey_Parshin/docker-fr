import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

// Define a service using a base URL and expected endpoints
export const topApi = createApi({
  reducerPath: "top",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_URL,
  }),
  endpoints: ({ query }) => ({
    smi: query({
      query: (body) => ({
        url: "/inpd/getSmiTop",
        method: "post",
        credentials: "include",
        // body,
      }),
    }),
    smiGraph: query({
      query: (body) => ({
        url: "/inpd/getSmiGraph",
        method: "post",
        credentials: "include",
        // body,
      }),
    }),
    soc: query({
      query: (body) => ({
        url: "/inpd/getSocTop",
        method: "post",
        credentials: "include",
        // body: { id },
      }),
    }),
    socGraph: query({
      query: (body) => ({
        url: "/inpd/getSocGraph",
        method: "post",
        credentials: "include",
        // body,
      }),
    }),
    unatedGraph: query({
      query: (body) => ({
        url: "/inpd/getUnatedGraph",
        method: "post",
        credentials: "include",
        // body,
      }),
    }),
    indicators: query({
      query: (body) => ({
        url: "/inpd/getIndicators",
        method: "post",
        credentials: "include",
        // body: { id },
      }),
    }),
    trustHourly: query({
      query: (body) => ({
        url: "stats/trusthourly",
        method: "post",
        credentials: "include",
        body,
      }),
    }),
    topStats: query({
      query: (body) => ({
        url: "/stats",
        method: "post",
        credentials: "include",
        body,
      }),
    }),
    postCount: query({
      query: (body) => ({
        url: "/content/getpostcount",
        method: "post",
        credentials: "include",
        body,
      }),
    }),
    postCountWeek: query({
      query: (body) => ({
        url: "/content/getweektrust",
        method: "post",
        credentials: "include",
        body,
      }),
    }),
    getAllPosts: query({
      query: (body) => ({
        url: "/threads/get",
        method: "post",
        credentials: "include",
        body,
      }),
    }),
    getAllParams: query({
      query: (body) => ({
        url: "/content/getweektrust",
        method: "post",
        credentials: "include",
        body,
      }),
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {
  useSmiQuery: useSmiTopQuery,
  useSmiGraphQuery,
  useSocQuery: useSocTopQuery,
  useSocGraphQuery,
  useUnatedGraphQuery,
  useTopStatsQuery,
  useTrustHourlyQuery,
  usePostCountQuery,
  usePostCountWeekQuery,
  useGetAllPostsQuery,
  useGetAllParamsQuery,
  useIndicatorsQuery: useIndicatorsTopQuery,
} = topApi;
