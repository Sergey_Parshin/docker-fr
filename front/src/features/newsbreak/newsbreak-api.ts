import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

// Define a service using a base URL and expected endpoints
export const newsbreakApi = createApi({
  reducerPath: "newsbreak",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_URL,
  }),
  endpoints: ({ query }) => ({
    list: query({
      query: (body) => ({
        url: "inpd/getInpdList",
        method: "post",
        credentials: "include",
        body,
      }),
    }),
    item: query({
      query: (id: number) => ({
        url: "inpd/getInpd",
        method: "post",
        credentials: "include",
        body: { id },
      }),
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const {
  useListQuery: useNewsbreakListQuery,
  useItemQuery: useNewsbreakItemQuery,
} = newsbreakApi;
