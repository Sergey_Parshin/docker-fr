import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

// Define a service using a base URL and expected endpoints
export const topicsApi = createApi({
  reducerPath: "topics",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_URL,
  }),
  endpoints: ({ query }) => ({
    topics: query({
      query: (body) => ({
        url: "stats/getReferenceSourceTopics",
        method: "post",
        credentials: "include",
        body,
      }),
    }),
    refSources: query({
      query: (body) => ({
        url: "inpd/getRefSources",
        method: "post",
        credentials: "include",
        body,
      }),
    }),
    topicRating: query({
      query: () => ({
        url: "inpd/getTopicRating",
        method: "post",
        credentials: "include",
      }),
    }),
    topicRatingShortRange: query({
      query: () => ({
        url: "inpd/getTopicRatingShortRange",
        method: "post",
        credentials: "include",
      }),
    }),
    topicsModal: query({
      query: (body) => ({
        url: "content/getTopic",
        method: "post",
        credentials: "include",
        body,
      }),
    }),
    smi: query({
      query: () => ({
        url: "content/currentsmi",
        method: "post",
        credentials: "include",
      }),
    }),
    mainTopic: query({
      query: (body) => ({
        url: "stats/getMainTopics",
        method: "post",
        credentials: "include",
        body,
      }),
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useTopicsQuery, useRefSourcesQuery, useTopicRatingQuery, useTopicRatingShortRangeQuery,  useTopicsModalQuery, useSmiQuery, useMainTopicQuery } = topicsApi;
