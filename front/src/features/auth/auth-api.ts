import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import { logIn, logOut } from "./auth-slice";

// Define a service using a base URL and expected endpoints
export const authApi = createApi({
  reducerPath: "auth/api",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_URL,
  }),
  endpoints: ({ mutation }) => ({
    logIn: mutation({
      query: (data) => ({
        url: `/authorization/login`,
        method: "POST",
        credentials: "include",
        body: data,
      }),
      async onQueryStarted(id, { dispatch, queryFulfilled }) {
        try {
          const response = await queryFulfilled;
          const id = response.data.uid;
          dispatch(logIn(id));
        } catch (err) {
          dispatch(logOut());
        }
      },
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useLogInMutation } = authApi;
