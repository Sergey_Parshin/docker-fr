import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

// Define a service using a base URL and expected endpoints
export const monitoringApi = createApi({
  reducerPath: "monitoring",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_URL,
  }),
  endpoints: ({ query }) => ({
    stats: query({
      query: () => ({
        url: "users/getreferences",
        method: "post",
        credentials: "include",
      }),
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useStatsQuery } = monitoringApi;
