export const toLocaleDate = (str: string) => {
  return new Date(str).toLocaleDateString();
};
export const toLocaleDateTime = (str: string) => {
  return new Date(str).toLocaleString();
};
