import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";
import {logIn, logOut} from '../auth'

// Define a service using a base URL and expected endpoints
export const threadsApi = createApi({
  reducerPath: "threads",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_URL,
  }),
  endpoints: ({ query, mutation }) => ({
    threads: query({
      query: () => ({
        url: "threads/get",
        credentials: "include",
      }),
    }),
    threadItem: query({
      query: (body) => ({
        url: "thread/getthreadextended",
        method: "POST",
        credentials: "include",
        body,
      }),
    }),
    checkAuth: mutation({
      query: (body) => ({
        url: "/authorization/login_get",
        method: "post",
        credentials: "include",
        body,
      }),
      async onQueryStarted(id, { dispatch, queryFulfilled }) {
        try {
          const response = await queryFulfilled;
          const id = response.data.uid;
          dispatch(logIn(id));
        } catch (err) {
          dispatch(logOut());
          window.location.href = `${process.env.REACT_APP_API_URL_SHORT}/slogin/provider/isiao/auth?front_redirect=${window.location.toString().slice(0,-5)}`
        }
      },
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { useThreadsQuery, useThreadItemQuery, useCheckAuthMutation } = threadsApi;
