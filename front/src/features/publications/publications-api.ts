import { createApi, fetchBaseQuery } from "@reduxjs/toolkit/query/react";

// Define a service using a base URL and expected endpoints
export const publicationsApi = createApi({
  reducerPath: "publications",
  baseQuery: fetchBaseQuery({
    baseUrl: process.env.REACT_APP_API_URL,
  }),
  endpoints: ({ query }) => ({
    publications: query({
      query: (body) => ({
        url: "content/posts",
        credentials: "include",
        method: "POST",
        body,
      }),
    }),
  }),
});

// Export hooks for usage in functional components, which are
// auto-generated based on the defined endpoints
export const { usePublicationsQuery } = publicationsApi;
