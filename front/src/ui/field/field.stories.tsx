import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Field } from "./field";

type Story = ComponentStory<typeof Field>;

export default {
  title: "Field",
  component: Field,
  decorators: [(Story) => <Story />],
} as ComponentMeta<typeof Field>;

export const Default: Story = () => <Field type="text" />;
export const WithIcon: Story = () => <Field type="text" icon="search" />;
