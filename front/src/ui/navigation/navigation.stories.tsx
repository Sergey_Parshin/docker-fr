import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Navigation } from "./navigation";
import logo from "../../features/layout/navigation/icons/logo-min.svg";
import { itemsNav } from "../../features/layout/navigation";
import { MemoryRouter } from "react-router-dom";

type Story = ComponentStory<typeof Navigation>;

export default {
  title: "Navigation",
  component: Navigation,
  decorators: [
    (Story) => (
      <MemoryRouter>
        <Story />
      </MemoryRouter>
    ),
  ],
} as ComponentMeta<typeof Navigation>;

export const Default: Story = () => (
  <Navigation
    className="s.nav"
    title="Мониторинг"
    items={itemsNav}
  />
);
