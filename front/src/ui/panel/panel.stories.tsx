// @ts-ignore
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Panel } from "./panel";

type Story = ComponentStory<typeof Panel>;

export default {
  title: "Panel",
  component: Panel,
} as ComponentMeta<typeof Panel>;

export const Default: Story = () => <Panel>Button</Panel>;
export const Padding: Story = () => <Panel padding>Button</Panel>;
export const PaddingCoat: Story = () => (
  <Panel padding coat>
    Button
  </Panel>
);
export const Number: Story = () => <Panel number={2}>Button</Panel>;
export const PaddingNumber: Story = () => (
  <Panel padding number={2}>
    Button
    <br />
    Button
    <br />
    Button
  </Panel>
);
export const PaddingNumberCoat: Story = () => (
  <Panel padding coat number={2}>
    Button
  </Panel>
);
