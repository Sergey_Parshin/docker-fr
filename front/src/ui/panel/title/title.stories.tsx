import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Title } from "./title";

type Story = ComponentStory<typeof Title>;

export default {
  title: "Title",
  component: Title,
} as ComponentMeta<typeof Title>;

export const Default: Story = () => <Title caption="Показатели" />;
