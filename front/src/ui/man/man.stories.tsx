import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Man } from "./man";

type Story = ComponentStory<typeof Man>;

export default {
  title: "Man",
  component: Man,
} as ComponentMeta<typeof Man>;

export const level0: Story = () => <Man level={0} />;
export const level30: Story = () => <Man level={30} />;
export const level60: Story = () => <Man level={60} />;
export const level100: Story = () => <Man level={100} />;
