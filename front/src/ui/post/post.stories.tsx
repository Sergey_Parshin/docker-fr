import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Post } from "./post";

type Story = ComponentStory<typeof Post>;

const statistic = {
  people: 3327,
  views: 1823,
  likes: 64,
  comments: 6,
  reposts: 27,
};

const title = "nw24.ru";
const text =
  "У исторического здания бывшего старейшего в России завода по производству резиновой обуви нет нормального хозяина, не считая государство, которое не способно сделать ничего хорошего. Более того, у Петербурга сейчас толком нет хозяина.";

const author = {
  name: "qwe",
  avatar: "",
  url: "https://yandex.ru",
};

const media = {
  images: [
    {url: "https://sun9-63.userapi.com/impf/c841427/v841427299/49e59/d3TSz4Ej5RQ.jpg?size=1536x1023&quality=96&sign=580f106db363f2e0293a1f62231b7158&c_uniq_tag=sTKKBLjPxWCg1pvYGW7__UAMSXFBDT7DxRSoIHseWXc&type=album"
  }
  ],
  video: [
    {url: "https://sun9-63.userapi.com/impf/c841427/v841427299/49e59/d3TSz4Ej5RQ.jpg?size=1536x1023&quality=96&sign=580f106db363f2e0293a1f62231b7158&c_uniq_tag=sTKKBLjPxWCg1pvYGW7__UAMSXFBDT7DxRSoIHseWXc&type=album"
    }
  ],
};

const id = '14566792';

export default {
  title: "Post",
  component: Post,
} as ComponentMeta<typeof Post>;

export const Default: Story = () => (
  <Post
    date={new Date()}
    coat={true}
    id={id}
    title={title}
    author={author}
    text={text}
    statistics={statistic}
    type="default"
    url="test.url"
    uri="test.uri"
    media={media}
  />
);

export const Positive: Story = () => (
  <Post
    date={new Date()}
    coat={false}
    id={id}
    title={title}
    author={author}
    text={text}
    statistics={statistic}
    type="positive"
    url="test.url"
    uri="test.uri"
    media={media}
  />
);

export const Negative: Story = () => (
  <Post
    date={new Date()}
    coat={true}
    id={id}
    title={title}
    author={author}
    text={text}
    statistics={statistic}
    type="negative"
    url="test.url"
    uri="test.uri"
    media={media}
  />
);
