import clsx from "clsx";
import { Panel } from "../panel";
import open from "./img/icon-open.svg";
import emoji from "./img/icon-emoji.svg";
import remove from "./img/icon-remove.svg";
import add from "./img/icon-add.svg";
import s from "./post-actions.module.css";

export type PostActionsProps = {
  className?: string;
  onReadClick: Function;
  onToneClick: Function;
  onExcludeClick: Function;
  onCreateClick: Function;
};

export function PostActions(props: PostActionsProps) {
  const { className, onReadClick, onToneClick, onExcludeClick, onCreateClick } =
    props;

  return (
    <Panel>
      <div className={clsx(className, s.postactions)}>
        <button onClick={() => onReadClick()} className={s.icon} type="button">
          <img src={open} alt="" />
        </button>
        <button onClick={() => onToneClick()} className={s.icon} type="button">
          <img src={emoji} alt="" />
        </button>
        <button
          onClick={() => onExcludeClick()}
          className={s.icon}
          type="button"
        >
          <img src={remove} alt="" />
        </button>
        <button
          onClick={() => onCreateClick()}
          className={s.icon}
          type="button"
        >
          <img src={add} alt="" />
        </button>
      </div>
    </Panel>
  );
}
