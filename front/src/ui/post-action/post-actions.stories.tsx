import { ComponentStory, ComponentMeta } from "@storybook/react";
import { PostActions } from "./post-actions";

type Story = ComponentStory<typeof PostActions>;

export default {
  title: "PostActions",
  component: PostActions,
} as ComponentMeta<typeof PostActions>;

const log = (name: string) => () => console.info(`🔥 name`, name);

export const Default: Story = () => (
  <PostActions
    onCreateClick={log("onCreateClick")}
    onExcludeClick={log("onExcludeClick")}
    onReadClick={log("onReadClick")}
    onToneClick={log("onToneClick")}
  />
);
