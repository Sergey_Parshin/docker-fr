// @ts-ignore
import { ComponentStory, ComponentMeta } from "@storybook/react";

import { Table } from "./table";

type Story = ComponentStory<typeof Table>;

export default {
  title: "Table",
  component: Table,
} as ComponentMeta<typeof Table>;

const tableData = [
  {
    title: "Название",
    subtitle: "Количество публикаций",
    figures: [100, 50, 0, 50],
  },
  {
    title: "Всего",
    subtitle: "Количество комментариев",
    figures: [1000, 400, 0, 600],
  },
  { title: "Поз.", subtitle: "Количество репостов", figures: [500, 0, 500, 0] },
  {
    title: "Нейт.",
    subtitle: "Количество лайков",
    figures: [600, 200, 0, 400],
  },
  {
    title: "Негат.",
    subtitle: "Количество просмотров",
    figures: [700, 200, 300, 200],
  },
];

export const Default: Story = () => <Table data={tableData} />;
