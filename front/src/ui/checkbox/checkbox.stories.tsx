import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Checkbox } from "./checkbox";

type Story = ComponentStory<typeof Checkbox>;

export default {
  title: "Checkbox",
  component: Checkbox,
} as ComponentMeta<typeof Checkbox>;

export const Default: Story = () => (
  <>
    <Checkbox checked={false} />
    <Checkbox checked={true} />
  </>
);
