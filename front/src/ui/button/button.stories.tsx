import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Button } from "./button";

type Story = ComponentStory<typeof Button>;

export default {
  title: "Button",
  component: Button,
} as ComponentMeta<typeof Button>;

export const Default: Story = () => <Button className="Button">Субъект</Button>;

export const Active: Story = () => (
  <Button className="Button" active={true}>
    Субъект
  </Button>
);
