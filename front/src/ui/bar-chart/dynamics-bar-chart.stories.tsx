// @ts-ignore
import { ComponentStory, ComponentMeta } from "@storybook/react";
import { DynamicsBarChart } from "./dynamics-bar-chart";

type Story = ComponentStory<typeof DynamicsBarChart>;

const data = [
  {
    name: "Page A",
    uv: 4000,
  },
  {
    name: "Page B",
    uv: 3000,
  },
  {
    name: "Page C",
    uv: 2000,
  },
  {
    name: "Page D",
    uv: 2780,
  },
  {
    name: "Page E",
    uv: 1890,
  },
  {
    name: "Page F",
    uv: 2390,
  },
  {
    name: "Page G",
    uv: 3490,
  },
];

export default {
  title: "DynamicsBarChart",
  component: DynamicsBarChart,
} as ComponentMeta<typeof DynamicsBarChart>;

export const Default: Story = () => (
  <DynamicsBarChart dataKey="uv" color="aqua" data={data} height={200} />
);
