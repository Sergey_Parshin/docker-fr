import type { Data } from "../../features";
import clsx from "clsx";
import { Panel } from "../panel";
import { Man } from "../man";
import s from "./post-media.module.css";
import { PublicationsChart } from "../../features";
import { TopicDescription } from "../topic-description";
import { Bar, BarChart, Tooltip, XAxis, YAxis } from "recharts";
import {useEffect} from 'react'

type Props = {
  number: number;
  data: PostMediaData;
  className?: string;
  coat?: boolean;
};

export type PostMediaData = {
  id: string;
  group_id: string;
  subject: string;
  level: number;
  topic: string;
  subjectPublications: number;
  totalPublications: number;
  subjectCoverage: number;
  totalCoverage: number;
  dynamicData: Data[];
  mediaPublications?: number;
  socialMediaPublications?: number;
  storyDocs?: number;
  lastHourDocs?: number;
  publications: any;
};

export function PostMedia({ className, number, coat = false, data }: Props) {
  const {
    group_id,
    subject,
    level,
    topic,
    subjectPublications,
    subjectCoverage,
    mediaPublications,
    totalPublications,
    totalCoverage,
    socialMediaPublications,
    dynamicData,
    storyDocs,
    lastHourDocs,
    publications
  } = data;

  let graphData = dynamicData

  if (dynamicData?.length > 48) {
    const dateSet = new Set(dynamicData.map(it => it.item_date.split(' ')[0]))
    graphData = []
    dateSet.forEach(it => {
      let postCount = 0
      dynamicData.filter(item => item.item_date.includes(it)).forEach(item => {
        postCount += item.post_count
      })
      const newDate = it.split(' ')[0]
      graphData.push({
        item_date: `date ${newDate.split('-')[1]}-${newDate.split('-')[2]}`,
        post_count: postCount
      })
    })
  }

  console.log('dynamicData', dynamicData)
  return (
    <Panel padding coat={coat} number={number} className={className}>
      <div className={s.media}>
        <div className={s.theme}>
          <p className={s.theme__caption}>Тема:</p>
          <h2 className={s.theme__title}>{topic}</h2>
          {subject && (
            <>
              <p className={s.person__caption}>Cубъект:</p>
              <p className={s.person__name}>{subject ?? "–"}</p>
            </>
          )}

          <div className={s.data}>
            <div className={s.dataCol}>
              {subjectPublications && subjectPublications !== 0 && (
                <TopicDescription
                  title="Публикаций с упоминанием субъекта"
                  description={subjectPublications}
                  coverage={subjectCoverage}
                />
              )}
              {totalPublications && totalPublications !== 0 && (
                <TopicDescription
                  title="Всего публикаций в теме"
                  description={totalPublications}
                  coverage={totalCoverage}
                />
              )}
              {storyDocs && storyDocs !== 0 && (
                <TopicDescription
                  title="Количество уникальных источников"
                  description={publications.postcount}
                  plus={lastHourDocs}
                />
              )}
            </div>
            {/*<div className={s.dataCol}>*/}
            {/*{mediaPublications && mediaPublications!==0 &&*/}
            {/*  <TopicDescription*/}
            {/*    title="Количество публикаций в СМИ"*/}
            {/*    description={mediaPublications}*/}
            {/*  />*/}
            {/*}*/}
            {/*{socialMediaPublications != null &&*/}
            {/*  <TopicDescription*/}
            {/*    title="Количество публикаций в cоц.сетях"*/}
            {/*    description={socialMediaPublications}*/}
            {/*  />*/}
            {/*}*/}
            {/*</div>*/}
          </div>
        </div>
        {graphData && (
          <div className={s.diagrams}>
            {/*<div className={clsx(s.diagram, { "not-ready": !dynamicData })}>*/}
            <div className={s.diagram}>
              <p>Динамика публикаций:</p>
              <Panel className={clsx(s.diagramPanel)}>
                {graphData && <PublicationsChart data={graphData} />}
              </Panel>
              <Panel padding className={s.panel}>
                <BarChart
                  className={s.barChart}
                  data={graphData.map((it: any) => ({
                    Публикаций: it.value,
                    name: it.name,
                  }))}
                  height={160}
                >
                  <Bar dataKey="Публикаций" fill="#4EC0E4" />
                  <Tooltip />
                  <XAxis
                    angle={-70}
                    dataKey="name"
                    textAnchor="end"
                    interval={0}
                    height={86}
                  />
                  <YAxis allowDecimals={false} minTickGap={1} />
                </BarChart>
              </Panel>
            </div>

            {/*<div className={clsx(s.diagram, "not-ready")}>*/}
            {/*  <p>Тональность публикаций:</p>*/}
            {/*  <Panel className={s.diagramPanel} padding>*/}
            {/*    GRAPHIC*/}
            {/*  </Panel>*/}
            {/*</div>*/}
          </div>
        )}
      </div>
    </Panel>
  );
}
