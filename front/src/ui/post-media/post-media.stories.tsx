import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Data } from "../../features";
import { PostMedia } from "./post-media";
type Story = ComponentStory<typeof PostMedia>;

export default {
  title: "PostMedia",
  component: PostMedia,
} as ComponentMeta<typeof PostMedia>;

const postMediaData = {
  id: 'dgst4tw4g',
  group_id: 'dgst4tw4g',
  subject: "Беглов",
  level: 30,
  topic:
    "Беглов официально уволил Батанова с поста финансового вице-губернатора Петербурга",
  subjectPublications: 19,
  totalPublications: 19,
  subjectCoverage: 3045840,
  totalCoverage: 3045840,
  mediaPublications: 14,
  socialMediaPublications: 5,
  publications:[],
  dynamicData: [
    {item_date: "2022-03-25 00:00", post_count: 2},
    {item_date: "2022-03-26 00:00", post_count: 1},
    {item_date: "2022-03-27 00:00", post_count: 3},
    {item_date: "2022-03-28 00:00", post_count: 4}
  ] as Data[],
};

export const Default: Story = () => (
  <PostMedia number={1} data={postMediaData} />
);
