import { ComponentStory, ComponentMeta } from "@storybook/react";

import { StatsPanel } from "./stats-panel";

type Story = ComponentStory<typeof StatsPanel>;

export default {
  title: "StatsPanel",
  component: StatsPanel,
} as ComponentMeta<typeof StatsPanel>;

export const Default: Story = () => (
  <StatsPanel
    className="statsPanel"
    title="Всего субъектов мониторинга"
    count={777}
  />
);
