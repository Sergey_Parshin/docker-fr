import { ComponentStory, ComponentMeta } from "@storybook/react";
import { Statistics } from "./statistics";

type Story = ComponentStory<typeof Statistics>;

export default {
  title: "Statistics",
  component: Statistics,
} as ComponentMeta<typeof Statistics>;

const data = {
  people: 3327,
  views: 1823,
  likes: 64,
  comments: 6,
  reposts: 27,
};

export const Default: Story = () => <Statistics data={data} />;
