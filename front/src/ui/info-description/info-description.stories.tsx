import { ComponentStory, ComponentMeta } from "@storybook/react";
import { InfoDescription } from "./info-description";

type Story = ComponentStory<typeof InfoDescription>;

export default {
  title: "InfoDescription",
  component: InfoDescription,
} as ComponentMeta<typeof InfoDescription>;

export const Default: Story = () => (
  <InfoDescription
    title="Региональный проект:"
    description="Культурная среда в Санкт-Петербурге"
  />
);
