import { ComponentStory, ComponentMeta } from "@storybook/react";
import { useState } from "react";
import { DateRangePicker } from "./date-range-picker";

type Story = ComponentStory<typeof DateRangePicker>;

export default {
  title: "DateRangePicker",
  component: DateRangePicker,
} as ComponentMeta<typeof DateRangePicker>;

export const Default: Story = () => {
  const [startDate, setStartDate] = useState<Date | null>(null);
  const [endDate, setEndDate] = useState<Date | null>(null);

  return (
    <DateRangePicker
      startDate={startDate}
      endDate={endDate}
      onChange={([startDate, endDate]) => {
        console.info(`🔥 startDate, endDate`, startDate, endDate);
        setStartDate(startDate);
        setEndDate(endDate);
      }}
    />
  );
};
