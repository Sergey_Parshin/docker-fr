import { ComponentStory, ComponentMeta } from "@storybook/react";

import { InfoPanel } from "./info-panel";

type Story = ComponentStory<typeof InfoPanel>;

export default {
  title: "InfoPanel",
  component: InfoPanel,
} as ComponentMeta<typeof InfoPanel>;

export const Default: Story = () => (
  <InfoPanel
    className="InfoPanel"
    title="Негатив со стороны градо- и зоозащитников, активистов и жителей, живущих у таврического сада"
    number={1}
    statistics={{
      publications: 550,
      people: 3327,
      views: 1823,
      likes: 64,
      comments: 6,
      reposts: 27,
    }}
  >
    Content
  </InfoPanel>
);
