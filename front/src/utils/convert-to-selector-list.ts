export const convertToSelectorList = (list: any[], fieldName: string): {id: string, keyword: string}[] => {
  const result = [{ id: '', keyword: 'Все'}]
  const newList = [...list].filter((it: any) => it.details[fieldName] && (typeof it.details[fieldName] === 'string' || it.details[fieldName].title)).map((it: any) => typeof it.details[fieldName] === 'string' ? it.details[fieldName] : it.details[fieldName].title)
  new Set(newList).forEach((it: string) => {
    result.push({ id: it, keyword: it })
  })
  return result
}
