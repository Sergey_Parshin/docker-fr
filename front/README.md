# Monitoring Application

Figma — https://www.figma.com/file/2s9LXUcERJKk24LV6Ze08y/Untitled

Предыдущая версия (логин и пароль те-же что и тут) — https://tsr.glassen-it.com/feed-by-posts

Все методы API — https://isiao.glassen-it.com/api/backend/


## API

Всего событий мониторинга, всего субъектов мониторинга\
`/component/socparser/users/getreferences`

Темы дня: СМИ и соцсети\
`/component/socparser/stats/getReferenceSourceTopics`

Публикации\
`/component/socparser/content/posts` - вывод публикаций

Быстрорастущие темы\
`/component/socparser/stats/getReferenceSourceTopics`

Новости Спб\
`/component/socparser/content/currentsmi`

Риски и инфоповоды (документация)\
https://isiao.glassen-it.com/api/backend/#operation/getInpdList\
https://isiao.glassen-it.com/api/backend/#operation/getRiskList\
https://isiao.glassen-it.com/api/backend/#operation/getInpd\
https://isiao.glassen-it.com/api/backend/#operation/getRisk\

Публикации для темы
/component/socparser/content/getTopic

Исключить пост — `thread/addstoppost`\
`{"thread_id":5472,"post_id":"12162","owner_id":-83053957,"network_id":1}`

Главная таблица показатели\
https://isiao.glassen-it.com/api/backend/#operation/getIndicators

Это топы для главного экрана\
https://isiao.glassen-it.com/api/backend/#operation/getSmiTop\
https://isiao.glassen-it.com/api/backend/#operation/getSocTop


